package italiano;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JList;

public class Family extends Dictionary implements ActionListener {
	
	public static Map<String,String> wordMap;	// dzi�ki temu,�e jest static,mamy dost�p do pola klasy bez tworzenia obiektu, tylko poprzez NazwaKlasy.poleStatyczne
	private JList<String> lista;
	private Quiz1Family quiz;
	
	public Family() {
		super();
		setTitle("Family");
		
		// the map of words
		wordMap = new LinkedHashMap<String, String>();	//LinkedHashMap keeps insertion order
		wordMap.put("la famiglia", "family");		
		wordMap.put("la madre", "mother");
		wordMap.put("il padre", "father");
		wordMap.put("la sorella", "sister");
		wordMap.put("il fratello", "brother");
		wordMap.put("la figlia", "doughter");
		wordMap.put("il figlio", "son");		
		wordMap.put("la moglie", "wife");			
		wordMap.put("il marito", "husband");
		wordMap.put("la nonna", "grandmother");
		wordMap.put("il nonno", "grandfather");
		wordMap.put("la zia", "aunt");
		wordMap.put("il zio", "uncle");
		wordMap.put("il matrimonio", "marriage");
		wordMap.put("fidanzato", "engadged");
		wordMap.put("ragazzo", "boyfriend");
		wordMap.put("ragazza", "girlfriend");
		wordMap.put("nipote", "nephew");
		
		lista = new JList<String>(super.makeWordsArray(wordMap));
		lista.setBounds(50, 80, 380, 400);
		add(lista);
		
		getbCheckYourself().addActionListener(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		Object source = e.getSource();
		if (source == getbCheckYourself()) {
			quiz = new Quiz1Family();
			quiz.setVisible(true);
			dispose();
		}
	
		
	}

}
