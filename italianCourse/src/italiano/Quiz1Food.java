package italiano;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Quiz1Food extends Quiz1 implements ActionListener {
	
	private Course quiz;
	private Course food;

	public Quiz1Food() {
		super();
		
		getbNext().addActionListener(this);
		getbPrevious().addActionListener(this);
		getbCheckOut().addActionListener(this);
		
		createQuiz(Food.wordMap);
	}
	
	@Override
	 public void actionPerformed(ActionEvent userActivity) {
		 Object source = userActivity.getSource();
		
		 if (source == getbCheckOut()) {
			 checkAnswers(Food.wordMap);
			 showResult();
		 
		 } else if (source == getbNext()) {
			 quiz = new Quiz2Food();
			 quiz.setVisible(true);
			 dispose();
		
		 } else if (source == getbPrevious()) {
			 food = new Food();
			 food.setVisible(true);
			 dispose();
		 }
			 
	 }

}
