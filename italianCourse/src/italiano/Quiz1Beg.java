package italiano;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Quiz1Beg extends Quiz1 implements ActionListener {
	
	private Course quiz;
	private Course beginningCourse;
	
	public Quiz1Beg() {
		super();
		
		getbNext().addActionListener(this);
		getbPrevious().addActionListener(this);
		getbCheckOut().addActionListener(this);
		
		createQuiz(BeginningCourse.wordMap);
	}
	
	 @Override
	 public void actionPerformed(ActionEvent userActivity) {
		 Object source = userActivity.getSource();
		 if (source == getbCheckOut()) {
			 checkAnswers(BeginningCourse.wordMap);
			 showResult();
		 
		 } else if (source == getbNext()) {
				quiz = new Quiz2Beg();	// wi�z. polimorficzne
				quiz.setVisible(true);	// wywo�uje metod� obiektu typu referencji (Course, a w�a�ciwie JFrame) na rzecz typu obiektu
				dispose();

			} else if (source == getbPrevious()) {
				beginningCourse = new BeginningCourse();	// wi�z. polimorficzne
				beginningCourse.setVisible(true);			// wywo�uje metod� obiektu typu referencji (Course, a w�a�ciwie JFrame)
				dispose();
			}
			 
	 }
	
}
