package italiano;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Quiz2Family extends Quiz2 implements ActionListener {
	
	private Course quiz;

	public Quiz2Family() {
		super();
		
		getbNext().addActionListener(this);
		getbPrevious().addActionListener(this);
		getbCheckOut().addActionListener(this);
		
		createQuiz(Family.wordMap);
	}

	@Override
	public void actionPerformed(ActionEvent userActivity) {
		 Object source = userActivity.getSource();
		 if (source == getbCheckOut()) {
			 checkAnswers(Family.wordMap);
			 showResult();
		 
		 } else if (source == getbNext()) {
				finish();
			
			} else if (source == getbPrevious()) {
				quiz = new Quiz1Family();
				quiz.setVisible(true);
				dispose();
			}
			
		}
		
	

}
