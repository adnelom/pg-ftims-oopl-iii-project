package italiano;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JList;

public class BeginningCourse extends Dictionary implements ActionListener {
	
	private JList lista;
	private Quiz1Beg quiz;
	List <String> wordList;
	
	public static Map<String,String> wordMap;	// dzi�ki temu,�e jest static, mamy dost�p do pola klasy bez tworzenia obiektu, tylko poprzez NazwaKlasy.poleStatyczne
	
			
	public BeginningCourse() {
		super();
		setTitle("Very beginning");
		
		// the map of words
		wordMap = new LinkedHashMap<String, String>();	//LinkedHashMap keeps insertion order
		wordMap.put("grazie", "thank you");
		wordMap.put("Buongiorno", "Good Morning");
		wordMap.put("a dopo", "see you");
		wordMap.put("mi scusi", "excuse me");
		wordMap.put("per favore", "please");
		wordMap.put("Arrivederci", "good bye");
		wordMap.put("ciao", "hi");
		wordMap.put("come stai?", "how are you?");
		wordMap.put("bene, e tu?", "fine, and you?");
		wordMap.put("Mi chiamo Marco", "my name is Marco");
		wordMap.put("Come ti chiami?", "what is your name?");
		wordMap.put("vengo dalla Polonia", "I come from Poland");
		wordMap.put("non parlo italiano", "I don't speak italian");
		wordMap.put("parli inglese?", "do you speak english?");
		wordMap.put("abito a danzica", "I live in Gdansk");
		wordMap.put("piacere di conoscerti", "nice to meet you");
		wordMap.put("di dove sei?", "where are you from?");
		wordMap.put("da dove vieni?", "where do you come from?");
		
		lista = new JList<String>(super.makeWordsArray(wordMap));
		lista.setBounds(50, 80, 380, 400);
		add(lista);
		
		getbCheckYourself().addActionListener(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		Object source = e.getSource();
		if (source == getbCheckYourself()) {
			quiz = new Quiz1Beg();
			quiz.setVisible(true);
			dispose();
		}
	}
	
}
	