package italiano;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class Quiz2Proffessions extends Quiz2 implements ActionListener {
	
	List<String> values;
	private Course quiz;
	
	public Quiz2Proffessions() {
		super();
		
		getbNext().addActionListener(this);
		getbPrevious().addActionListener(this);
		getbCheckOut().addActionListener(this);
		
		createQuiz(Professions.wordMap);
	}

	public void actionPerformed(ActionEvent userActivity) {
		 Object source = userActivity.getSource();
		 if (source == getbCheckOut()) {
			 checkAnswers(Professions.wordMap);
			 showResult();
		 
		 } else if (source == getbNext()) {
			 finish();
		 
		 } else if (source == getbPrevious()) {
			 quiz = new Quiz1Proffessions();
			 quiz.setVisible(true);
			 dispose();
		 }
		
	}


}

