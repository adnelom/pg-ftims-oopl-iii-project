package italiano;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Start extends JFrame implements ActionListener {
	
	private JLabel lHello;
	private JLabel lCourse;
	private JButton bBeginning;
	private JButton bProffesions;
	private JButton bFood;
	private JButton bFamily;
	private BeginningCourse beginningCourse;
	private Food food;
	private Professions proffessions;
	private Family family;
		
	public Start() {
		setSize(500, 500);
		setTitle("Italian 4 U");
		setLayout(null);
		
		lHello = new JLabel("Italian for you");
		lHello.setBounds(90, 50, 400, 150);
		lHello.setForeground(Color.RED);
		lHello.setFont(new Font("CosmicSand", Font.BOLD, 44));
		add(lHello);
		
		lCourse = new JLabel("Choose a course");
		lCourse.setBounds(200, 200, 150, 20);
		add(lCourse);
		
		bBeginning = new JButton("Very beginning");
		bBeginning.setBounds(50, 250, 170, 50);
		bBeginning.addActionListener(this);
		add(bBeginning);
		
		bProffesions = new JButton("Proffesions");
		bProffesions.setBounds(50, 320, 170, 50);
		bProffesions.addActionListener(this);
		add(bProffesions);
		
		bFood = new JButton("Food");
		bFood.setBounds(250, 250, 170, 50);
		bFood.addActionListener(this);
		add(bFood);
		
		bFamily = new JButton("Family");
		bFamily.setBounds(250, 320, 170, 50);
		bFamily.addActionListener(this);
		add(bFamily);
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == bBeginning) {
				beginningCourse = new BeginningCourse();
				beginningCourse.setVisible(true);
				dispose();
		} else if (source == bFood) {
			food = new Food();
			food.setVisible(true);
			dispose();
			
		} else if (source == bProffesions) {
			proffessions = new Professions();
			proffessions.setVisible(true);
			dispose();
			
		} else if (source == bFamily) {
			family = new Family();
			family.setVisible(true);
			dispose();
		}
		
	}
	
	public static void main(String[] args) {
		Start app = new Start();
		app.setDefaultCloseOperation(EXIT_ON_CLOSE);
		app.setVisible(true);
	}
	

}
