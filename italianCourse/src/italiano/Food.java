package italiano;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JList;

public class Food extends Dictionary implements ActionListener {
	
	public static Map<String,String> wordMap;	// dzi�ki temu, �e jest static, mamy dost�p do pola klasy bez tworzenia obiektu, tylko poprzez NazwaKlasy.poleStatyczne
	private JList<String> lista;
	private Quiz1Food quiz;
	
	public Food() {
		super();
		setTitle("Food");
		
		// the map of words
		wordMap = new LinkedHashMap<String, String>();	//LinkedHashMap keeps insertion order
		wordMap.put("il pane", "bread");
		wordMap.put("il burro", "butter");
		wordMap.put("il formaggio", "cheese");
		wordMap.put("la marmellata", "jam");
		wordMap.put("il miele", "honey");
		wordMap.put("il caffe'", "coffee");		
		wordMap.put("il te'","tee");			
		wordMap.put("il cornetto", "croissant");
		wordMap.put("le frutta", "fruits");
		wordMap.put("le verdura", "vegetables");
		wordMap.put("il fungo", "mushroom");
		wordMap.put("la mela", "apple");
		wordMap.put("il latte'", "milk");		
		wordMap.put("il pesce", "fish");
		wordMap.put("acqua naturale/frizzante", "still/sparkling water");
		wordMap.put("il carne", "meat");
		wordMap.put("la colazione", "breakfast");
		wordMap.put("la cena", "supper");
		
		lista = new JList<String>(super.makeWordsArray(wordMap));
		lista.setBounds(50, 80, 380, 400);
		add(lista);
		
		getbCheckYourself().addActionListener(this);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		Object source = e.getSource();
		if (source == getbCheckYourself()) {
			quiz = new Quiz1Food();
			quiz.setVisible(true);
			dispose();
		}
	}

}
