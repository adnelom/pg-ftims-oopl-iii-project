package italiano;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Quiz1Family extends Quiz1 implements ActionListener{

	private Course quiz;
	private Course family;

	public Quiz1Family() {
		super();
		
		getbNext().addActionListener(this);
		getbPrevious().addActionListener(this);
		getbCheckOut().addActionListener(this);
		
		createQuiz(Family.wordMap);
	}
	
	 @Override
	 public void actionPerformed(ActionEvent userActivity) {
		 Object source = userActivity.getSource();
		 if (source == getbCheckOut()) {
			 checkAnswers(Family.wordMap);
			 showResult();
		 
		 } else if (source == getbNext()) {
				quiz = new Quiz2Family();			// wi�z. polimorficzne
				quiz.setVisible(true);				// wywo�uj� metod� obiektu typu referencji (Course, a w�a�ciwie JFrame)
				dispose();

			} else if (source == getbPrevious()) {
				family = new Family();				// wi�z. polimorficzne
				family.setVisible(true);			// wywo�uj� metod� obiektu typu referencji (Course, a w�a�ciwie JFrame)
				dispose();
			}
			 
	 }
	
}



