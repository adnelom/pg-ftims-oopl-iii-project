package italiano;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Quiz2 extends Course implements InterfaceQuiz {
	
	private JButton bNext;
	private JButton bPrevious;
	private JButton bCheckOut;

	public Quiz2() {
		super();
		setTitle("Check yourself");
		
		setbNext(new JButton("Next"));
		getbNext().setBounds(400, 20, 80, 20);
		add(getbNext());

		setbPrevious(new JButton("Previous"));
		getbPrevious().setBounds(20, 20, 100, 20);
		add(getbPrevious());
		
		setbCheckOut(new JButton("Check out!"));
		getbCheckOut().setBounds(100, 600, 300, 50);
		add(getbCheckOut());
	}
	
	public JButton getbNext() {
		return bNext;
	}

	public void setbNext(JButton bNext) {
		this.bNext = bNext;
	}

	public JButton getbPrevious() {
		return bPrevious;
	}

	public void setbPrevious(JButton bPrevious) {
		this.bPrevious = bPrevious;
	}
	
	public JButton getbCheckOut() {
		return bCheckOut;
	}

	public void setbCheckOut(JButton bCheckOut) {
		this.bCheckOut = bCheckOut;
	}

	@Override
	public void createQuiz(Map<String, String> wordMap) {
		int y = 50;
		List<String> values = new ArrayList<String>(); 			// lista warto�ci (pyta�)
		for (String k : wordMap.keySet())
			values.add(wordMap.get(k));
		
		setMeanings(new ArrayList<JTextField>());		// list of text fields
		
		for (int i = 0; i < values.size(); i++) {
			JLabel lWord = new JLabel(values.get(i));
			lWord.setBounds(50, y, 200, 20);
			add(lWord);

			JTextField tMeaning = new JTextField();
			tMeaning.setBounds(200, y, 200, 20);
			add(tMeaning);
			getMeanings().add(tMeaning);

			y += 30;
		}
	}

	@Override
	public void checkAnswers(Map<String, String> wordMap) {
		List<String> keys = new ArrayList<String>();	// lista kluczy (odpowiedzi)
		keys.addAll(wordMap.keySet());
		
		for (int i = 0; i < keys.size(); i++) {
			if (getMeanings().get(i).getText().equalsIgnoreCase(keys.get(i))) {
				getMeanings().get(i).setForeground(Color.GREEN);
				setGoodAnswers(getGoodAnswers() + 1);
			} else
				getMeanings().get(i).setForeground(Color.RED);
		}
		
	}
	
	@Override
	public void finish() {
		int answer = JOptionPane.showConfirmDialog(null, "You have completed "
				+ "this course. Congratulation!\nDo you want to do another course?", 
				"Course completed", JOptionPane.YES_NO_OPTION); 
		if (answer == JOptionPane.YES_OPTION) {
			Start startFrame = new Start();
			startFrame.setVisible(true);
			dispose();
		}
		
	}
	
}
