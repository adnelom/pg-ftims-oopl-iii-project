package italiano;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Quiz2Food extends Quiz2 implements ActionListener {
	
	private Course quiz;

	public Quiz2Food() {
		super();
		
		getbNext().addActionListener(this);
		getbPrevious().addActionListener(this);
		getbCheckOut().addActionListener(this);
		
		createQuiz(Food.wordMap);
	}

	@Override
	public void actionPerformed(ActionEvent userActivity) {
		 Object source = userActivity.getSource();
		 if (source == getbCheckOut()) {
			 checkAnswers(Food.wordMap);
			 showResult();
		 
		 } else if (source == getbNext()) {
				finish();			
			 
		 } else if (source == getbPrevious()) {
			 quiz = new Quiz1Food();
			 quiz.setVisible(true);
			 dispose();
		 }
		
	}

}
