package italiano;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Quiz1 extends Course implements InterfaceQuiz {

	private JButton bNext;
	private JButton bPrevious;
	private JButton bCheckOut;

	public Quiz1() {
		super();
		setTitle("Check yourself");
		
		setbNext(new JButton("Next"));
		getbNext().setBounds(400, 20, 80, 20);
		add(getbNext());

		setbPrevious(new JButton("Previous"));
		getbPrevious().setBounds(20, 20, 100, 20);
		add(getbPrevious());
		
		setbCheckOut(new JButton("Check out!"));
		getbCheckOut().setBounds(100, 600, 300, 50);
		add(getbCheckOut());
		
	}
	
	public JButton getbNext() {
		return bNext;
	}

	public void setbNext(JButton bNext) {
		this.bNext = bNext;
	}

	public JButton getbPrevious() {
		return bPrevious;
	}

	public void setbPrevious(JButton bPrevious) {
		this.bPrevious = bPrevious;
	}
	
	public JButton getbCheckOut() {
		return bCheckOut;
	}

	public void setbCheckOut(JButton bCheckOut) {
		this.bCheckOut = bCheckOut;
	}

	@Override
	public void createQuiz(Map<String,String> wordMap) {
		int y = 50;
		setMeanings(new ArrayList<JTextField>());
		for (String w : wordMap.keySet()) {
			JLabel lWord = new JLabel(w);
			lWord.setBounds(50, y, 200, 20);
			add(lWord);

			JTextField tMeaning = new JTextField();
			tMeaning.setBounds(200, y, 200, 20);
			add(tMeaning);
			getMeanings().add(tMeaning);

			y += 30;
		}
		
	}

	@Override
	public void checkAnswers(Map<String,String> wordMap) {
		List<String> values = new ArrayList<String>(); // lista warto�ci (odpowiedzi)
		Iterator<String> it = wordMap.values().iterator();
		while (it.hasNext())
		values.add(it.next());
		
//		for (String k : BeginningCourse.wordMap.keySet())
//			values.add(BeginningCourse.wordMap.get(k));
		
		for (int i = 0; i < values.size(); i++) {
			if (getMeanings().get(i).getText().equalsIgnoreCase(values.get(i))) {
				getMeanings().get(i).setForeground(Color.GREEN);
			setGoodAnswers(getGoodAnswers() + 1);
			} else
				getMeanings().get(i).setForeground(Color.RED);
		}
		
	}

	@Override
	public void finish() {}
	// do nothing but must be overridden

}
