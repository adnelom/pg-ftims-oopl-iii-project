package italiano;

import java.util.Map;

public interface InterfaceQuiz {
	/**
	 * Creates a quiz that contains of JLabel form key of wordMap and JTextField form value
	 * of wordMap, or the other way round; put sequentially, one below the other
	 * @param wordMap determines the map of Strings that is used to create quiz. 
	 */
	void createQuiz(Map<String,String> wordMap);
	
	/**
	 * Checks if String inscribed in JTextField field matches to its key/value from JLabel field
	 * @param wordMap determines the map of Strings that is the pattern for checking
	 */
	void checkAnswers(Map<String,String> wordMap);
	
	/**
	 * displays the dialog box which informs about the completion of the course and asks 
	 * if the user want to remain on the last site of the course or go back to the courses
	 */
	void finish();

}
