"Italian for you"
Zuzanna Molenda


What does the program do?

"Italian for you" is a program designed to help learning Italian. 


How to compile and run application?

Push the buttons: Run Start -> Start.java


Additional information heeded for application usage

Choose a course to decide what kind of words you want to learn.
"DON'T PUSH THIS BUTTON" - you had better not push this button
"Check yourself" - go to the quiz, which will check if you already know all the words from the list
"Check out!" - check if your answears are correct. If it changes to green, it's correct; if it changes to red, it's incorrect. You will see your % result on dialog window