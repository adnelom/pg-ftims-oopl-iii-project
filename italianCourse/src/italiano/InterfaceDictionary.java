package italiano;

import java.util.Map;

public interface InterfaceDictionary {
	/**
	 * Converts wordMap from Map<String,String> into array String[]
	 * @param wordMap determines the map of Strings that is to be converted
	 * @return wordMap as an array
	 */
	public String[] makeWordsArray(Map<String, String> wordMap);
	
}
