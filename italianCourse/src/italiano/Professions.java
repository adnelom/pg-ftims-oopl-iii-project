package italiano;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JList;

public class Professions extends Dictionary implements ActionListener {
	
	public static Map<String,String> wordMap;	// dzi�ki temu,�e jest static,mamy dost�p do pola klasy bez tworzenia obiektu, tylko poprzez NazwaKlasy.poleStatyczne
	private JList<String> lista;
	private Quiz1Proffessions quiz;
	
	public Professions() {
		super();
		setTitle("Professions");
		
		// the map of words
		wordMap = new LinkedHashMap<String, String>();	//LinkedHashMap keeps insertion order
		wordMap.put("la professione", "profession");
		wordMap.put("l'insegnante", "teacher");
		wordMap.put("il dottore", "doctor");
		wordMap.put("la infermiera", "nurse");
		wordMap.put("l'avvocato", "lawyer");
		wordMap.put("informatico", "computer scientist");
		wordMap.put("il venditore", "seller");
		wordMap.put("il sacerdote", "priest");
		wordMap.put("il fisico", "physicist");
		wordMap.put("il chimico", "chemist");
		wordMap.put("il autista", "driver");
		wordMap.put("il assistente sociale", "social worker");
		wordMap.put("il pittore", "painter");
		wordMap.put("il artista", "artist");
		wordMap.put("il ingegnere", "engineer");
		wordMap.put("il capo", "boss");
		wordMap.put("il postino", "postman");
		wordMap.put("il poliziotto", "policeman");
		
		lista = new JList<String>(super.makeWordsArray(wordMap));
		lista.setBounds(50, 80, 380, 400);
		add(lista);
		
		getbCheckYourself().addActionListener(this);		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		Object source = e.getSource();
		if (source == getbCheckYourself()) {
			quiz = new Quiz1Proffessions();
			quiz.setVisible(true);
			dispose();
		}
	}
	

}
