package italiano;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Quiz1Proffessions extends Quiz1 implements ActionListener {

	private Course quiz;
	private Course proffessions;

	public Quiz1Proffessions() {
		super();
		
		getbNext().addActionListener(this);
		getbPrevious().addActionListener(this);
		getbCheckOut().addActionListener(this);
		
		createQuiz(Professions.wordMap);
	}
	
	 @Override
	 public void actionPerformed(ActionEvent userActivity) {
		 Object source = userActivity.getSource();
		 if (source == getbCheckOut()) {
			 checkAnswers(Professions.wordMap);
			 showResult();
		 
		 } else if (source == getbNext()) {
			 quiz = new Quiz2Proffessions();
			 quiz.setVisible(true);
			 dispose();
		 
		 } else if (source == getbPrevious()) {
			 proffessions = new Professions();
			 proffessions.setVisible(true);
			 dispose();
		 }
			 
	 }
	
}
