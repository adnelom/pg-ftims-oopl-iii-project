package italiano;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Quiz2Beg extends Quiz2 implements ActionListener {

	private Course quiz;
	
	public Quiz2Beg() {
		super();
		
		getbNext().addActionListener(this);
		getbPrevious().addActionListener(this);
		getbCheckOut().addActionListener(this);
		
		createQuiz(BeginningCourse.wordMap);
	}

	@Override
	public void actionPerformed(ActionEvent userActivity) {
		 Object source = userActivity.getSource();
		 if (source == getbCheckOut()) {
			 checkAnswers(BeginningCourse.wordMap);
			 showResult();
		 
		 } else if (source == getbNext()) {
				finish();
			
			} else if (source == getbPrevious()) {
				quiz = new Quiz1Beg();
				quiz.setVisible(true);
				dispose();
			}
			
		}
		
	


}
