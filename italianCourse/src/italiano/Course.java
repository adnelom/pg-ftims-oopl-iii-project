package italiano;

import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class Course extends JFrame {
	private List<JTextField> meanings;
	private int goodAnswers = 0;
	
	public Course() {
		setSize(500, 700);
		setLayout(null);
	}
	
	/**
	 * Displays your test result in the dialog box
	 */
	public void showResult() {
		double result = ((double)getGoodAnswers()/(double)getMeanings().size()) * 100.0;
		JOptionPane.showMessageDialog(null, "Your result is " + result + "%");
	}

	public int getGoodAnswers() {
		return goodAnswers;
	}

	public void setGoodAnswers(int goodAnswers) {
		this.goodAnswers = goodAnswers;
	}

	public List<JTextField> getMeanings() {
		return meanings;
	}

	public void setMeanings(List<JTextField> meanings) {
		this.meanings = meanings;
	}

}
