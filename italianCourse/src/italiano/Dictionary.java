package italiano;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JOptionPane;

public class Dictionary extends Course implements ActionListener, InterfaceDictionary {

	private JButton bPrevious;
	private JButton bBomba;
	private Start startFrame;
	private JButton bCheckYourself;

	public Dictionary() {
		super();
		
		bPrevious = new JButton("Back to courses");
		bPrevious.setBounds(20, 20, 150, 20);
		bPrevious.addActionListener(this);
		add(bPrevious);
		
		bBomba = new JButton("DON'T PUSH THIS BUTTON!");
		bBomba.setBounds(280, 500, 200, 20);
		bBomba.addActionListener(this);
		add(bBomba);
		
		setbCheckYourself(new JButton("Check yourself"));
		getbCheckYourself().setBounds(150, 550, 200, 50);
		add(getbCheckYourself());
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if (source == bBomba) {
			try {
				throw new MyException();
			} catch (MyException e1) {
				JOptionPane.showMessageDialog(this, e1.getMessage(),
						"Simple error in judgement", JOptionPane.ERROR_MESSAGE);
			}
		
		} else if (source == bPrevious) {
			startFrame = new Start();
			startFrame.setVisible(true);
			dispose();
		}
		
	}
	
	@Override
	public String[] makeWordsArray(Map<String, String> wordMap) {		
		// zamiana mapy na arrayList
		List<String> wordList = new ArrayList<String>();		
		for (String w : wordMap.keySet()) {
			wordList.add(w + " - " + wordMap.get(w));
		}
		// zamiana ArrayList na tablic� (JList czyta tylko tablice)
		String[] tabWord = wordList.toArray(new String[wordList.size()]);
		
		return tabWord;
	}

	public JButton getbCheckYourself() {
		return bCheckYourself;
	}

	public void setbCheckYourself(JButton bCheckYourself) {
		this.bCheckYourself = bCheckYourself;
	}


}
